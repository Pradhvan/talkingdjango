# talkingdjango
Raw django notes + coding samples from weekly reading/coding sprints. 


### File Structure
 
```bash
talkingdjango/
├── Mridu/
├── Pradhvan/
└── Shivam/ 
```

Top level file structure is to be kept the same, within your name folders do whatever you want to keep the file structure.
